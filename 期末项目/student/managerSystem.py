"""
这是主要的student模块的运行模块
"""
from 期末项目.student.student import Student


class StudentManager(object):
    def __init__(self):
        # 创建一个列表用来保存数据  列表就相当于我们java里面的list集合
        # 严格控制缩进，多一个空格都不行
        self.stu_list = []

    def run(self):
        # 加载文件数据

        while True:
            # 显示可选择的功能
            self.xianshi()

            """
                int(a) 表示把a转换成为整数类型
                input("请输入你想要的功能："): 相当于键盘输入语句，并且提示你要进行这样的输入
                但是键盘输入的数据默认是字符串类型，我们需要的是整数类型所以需要转换
            """

            xz1 = int(input("请输入你想要的功能："))
            if xz1 == 1:
                # pass表示我还没有想好具体的实现，所以先用pass代替避免程序出错
                self.stu_add()  #调用增加方法
            elif xz1 == 2:
                self.stu_del()
            elif xz1 == 3:
                self.stu_up()
            elif xz1 == 4:
                pass
            elif xz1 == 5:
                pass
            elif xz1 == 6:
                pass
            elif xz1 == 7:
                break # 退出程序

    def xianshi(self):
        print("1、增加学生功能")
        print("2、删除学生功能")
        print("3、修改学生功能")
        print("4、查询一个学生功能")
        print("5、查询全部学生功能")
        print("6、保存学生信息到文件功能")
        print("7、退出系统功能")

    # 增加功能代码
    def stu_add(self):
        name = input("请输入对象的姓名：")
        age = input("请输入对象的年龄：")
        gender = input("请输入对象的性别：")
        tel = input("请输入对象的电话号码：")
        student = Student(name,age,gender,tel)
        # 把增加的student对象保存到列表里面
        # append(数据)  把数据保存到列表里面
        self.stu_list.append(student)
        print("下面是列表内部的数据")
        """
            in 需要遍历的数据集
            i  就表示每一次遍历数据集内部的数据
        """
        for i in self.stu_list:
            print(i)

    # 删除的功能
    def stu_del(self):
        """
            1、键盘输入想要删除的姓名
            2、遍历列表，删除列表内部和想要删除姓名相同的对象
            3、删除列表内部对应的对象
            4、如果没有找到就输出“没找到你想要删除的对象”
            5、最后遍历列表查看操作之后的列表数据
            self.stu_list.remove(数据)
        """
        name = input("请输入想要删除的姓名：")
        for i in self.stu_list:
            if i.name == name:  # 找到列表内部和想要删除姓名相同的对象
                self.stu_list.remove(i)
            else:
                print("没找到你想要删除的对象")
        print("下面是列表内部的数据")
        for i in self.stu_list:
            print(i)

    # 修改功能
    def stu_up(self):
        """
        1、键盘输入想要修改的姓名
        2、遍历列表，修改列表内部和想要修改姓名相同的对象
        3、修改列表内部对应的对象
        4、如果没有找到就输出“没找到你想要修改的对象”
        5、最后遍历列表查看操作之后的列表数据
        """
        name = input("请输入想要修改的姓名：")
        nameup = input("请输入修改后的姓名：")
        ageup = input("请输入想修改的年龄：")
        genderup = input("请输入想修改的性别：")
        telup = input("请输入想修改的电话：")
        for i in self.stu_list:
            if i.name == name:  # 找到列表内部和想要修改姓名相同的对象
                # 给对象重新赋值
                i.name = nameup
                i.age = ageup
                i.gender = genderup
                i.tel = telup
            else:
                print("没找到你想要修改的对象")
        print("下面是列表内部的数据")
        for i in self.stu_list:
            print(i)