""""
这是student实体类
在python中我们的多行注释是使用
三对双引号来完成的
"""
# 单行注释是使用#号来完成的
"""
class  类名（父类名）：这是python中创建一个类的格式
"""

#   python里面通过缩进来控制代码的作用范围
class Student(object):  #   创建了一个Student类，继承了object类
    """
    在python中创建一个方法的格式   def 方法名（self）：
    __init__()方法就相当于我们java里面的构造方法，创建对象的时候就会调用这个方法
    self：表示当前类对象    相当于java里面的this
    """
    def __init__(self,name1,age1,gender1,tel1):
        #self.属性名 = 属性值 给类命名属性并且赋值
        self.name = name1
        self.age = age1
        self.gender = gender1
        self.tel = tel1

    # __str__()魔法方法，主要是用来输出对象的属性值  相当于java内部的toString方法
    def __str__(self):
        # 要使用变量的时候通过大括号来调用
        return (f"{self.name}\t{self.age}\t"
                f"{self.gender}\t{self.tel}")

#  python中创建对象的格式   对象名 = 类名（属性值）
student = Student("张三",18,"男","123456")
print(student)
#ctrl+shift+f10  运行当前模块的代码